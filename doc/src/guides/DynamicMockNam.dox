﻿namespace MockNetworkAccess {

/*!

\page page_dynamicMockNam Guide: Dynamic MockNetworkAccess::Manager

\brief This page explains how the MockNetworkAccess::Manager can be extended to have more dynamic behavior.

Out of the box, the predicates and the reply creation are stateless and rather static:
- For predicates, the values compared against the request have to be defined beforehand.
- For reply creation, the properties of the created reply have to be defined beforehand.

Although this can be solved by writing multiple rules (one Rule for each possible case), this is rather cumbersome,
does not allow maintaining state and can be impractical in complex scenarios due to the large number of very similar
rules (stay [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself)).


\section page_dynamicMockNam_dynamicPredicates Dynamic Predicates

Dynamic predicates allow complex matching patterns and stateful predicates.

There are two ways to create dynamic predicates:
- Using a \link MockNetworkAccess::Predicates::Generic Generic \endlink predicate, for example with a lambda.
- Creating a new Predicate class by deriving from Rule::Predicate.

\subsection page_dynamicMockNam_dynamicPredicates_examples Examples

\subsubsection page_dynamicMockNam_dynamicPredicates_examples_1 Stateful Predicate

This example shows a stateful predicate which matches every n-th request. It is implemented by deriving from Rule::Predicate.

Predicate Definition:
\snippet statefulPredicate.cpp Predicate Definition

Predicate Usage:
\snippet statefulPredicate.cpp Predicate Usage

\subsubsection page_dynamicMockNam_dynamicPredicates_examples_2 Complex Matching with "or" Semantics

This example shows a predicate which matches when the request contains authorization data using one of three methods:
- a header called `MyApp-Session` containing a predefined session id
- a cookie containing a predefined session id
- HTTP authorization header containing predefined user credentials

The predicate is implemented using a lambda and Predicates::Generic (using the shorthand method Rule::isMatching()).

\snippet genericPredicate.cpp main


\section page_dynamicMockNam_dynamicReplies Dynamic Replies

Dynamic replies allow introducing state in rules and incorporating the exact values of a request in the reply to create
different replies for requests matching the same predicates.

To create replies dynamically, derive from the Rule class and override the Rule::createReply() method.
To actually create replies, use a MockReplyBuilder.
Note that you can use the MockReplyBuilder returned by the Rule::reply() but be aware that changes to it are persistent
across `createReply()` calls.

The `createReply()` method also supports testing the request: if the rule should not create a reply for a given request,
return a null pointer (`Q_NULLPTR`) from createReply() and the Manager will treat it as if the rule's predicates didn't match.

\subsection page_dynamicMockNam_dynamicReplies_example Example

This example implements a Rule which behaves like an "echo" service replying with details of the request it matched.

\snippet dynamicReply.cpp Rule Definition


*/

} // namespace MockNetworkAccess
