#include "MockNetworkAccessManager.hpp"

int main( int argc, char *argv[] )
{
	//! [main]

	using namespace MockNetworkAccess;
	using namespace MockNetworkAccess::Predicates;

	const QString dummySessionId( "12345" );
	const QByteArray sessionHeader( "MyApp-Session" );
	const QString username( "john" );
	const QString password( "secret" );

	Rule requestWithSession;
	requestWithSession.has( UrlMatching( QRegularExpression( "https?://example.com/protected" ) ) )
	.isMatching( [=]( const Request& request ) -> bool {
		if ( request.qRequest.hasRawHeader( sessionHeader )
		  && request.qRequest.rawHeader( sessionHeader ) == dummySessionId.toLatin1() )
			return true;
		Cookie cookiePredicate( QNetworkCookie( sessionHeader, dummySessionId.toLatin1() ) );
		if ( cookiePredicate.matches( request ) )
			return true;
		Authorization authPredicate( username, password );
		if ( authPredicate.matches( request ) )
			return true;
		return false;
	}).reply().withBody( QJsonDocument::fromJson( "{\"protected\":\"data\"}" ) );

	//! [main]

	return 0;
}

