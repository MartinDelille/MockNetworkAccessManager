cmake_minimum_required( VERSION 3.3 )
if( POLICY CMP0071 )
	cmake_policy( SET CMP0071 NEW )
endif()

project( MockNetworkAccessManager )
set( PROJECT_VERSION 0.10.1 )
set( PROJECT_VERSION_PRERELEASE "" )

set( CMAKE_INCLUDE_CURRENT_DIR ON )
set( CMAKE_AUTOMOC ON )

set( CMAKE_CXX_STANDARD 11 )
set( CMAKE_CXX_STANDARD_REQUIRED ON )

option( FORCE_QT5 "Use Qt5 even if Qt6 is available. By default, Qt6 is preferred over Qt5." OFF )

set( QT_SEARCH_NAMES Qt6 Qt5 )
if( FORCE_QT5 )
	set( QT_SEARCH_NAMES Qt5 )
endif()
find_package( QT NAMES ${QT_SEARCH_NAMES} REQUIRED COMPONENTS Core Network )
find_package( Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core Network )

get_target_property( QTCORE_LOCATION Qt${QT_VERSION_MAJOR}::Core LOCATION )
get_filename_component( QT_BIN_DIR ${QTCORE_LOCATION} DIRECTORY )
set( CMAKE_MSVCIDE_RUN_PATH ${QT_BIN_DIR} )
set_property( DIRECTORY "." APPEND PROPERTY COMPILE_DEFINITIONS "QT_DEPRECATED_WARNINGS" )


if( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" )

	add_compile_options( "-Werror" "-Wall" "-Wextra" )
	set( CMAKE_CXX_FLAGS_COVERAGE "${CMAKE_CXX_FLAGS_DEBUG} -O0 --coverage -DQT_FORCE_ASSERTS" )
	set( CMAKE_C_FLAGS_COVERAGE "${CMAKE_C_FLAGS_DEBUG} -O0 --coverage -DQT_FORCE_ASSERTS" )
	set( CMAKE_EXE_LINKER_FLAGS_COVERAGE "${CMAKE_EXE_LINKER_FLAGS_DEBUG} --coverage" )
	set( CMAKE_SHARED_LINKER_FLAGS_COVERAGE "${CMAKE_SHARED_LINKER_FLAGS_DEBUG} --coverage" )
	set( CMAKE_STATIC_LINKER_FLAGS_COVERAGE "${CMAKE_STATIC_LINKER_FLAGS_DEBUG}" )
	get_property( QT_IMPORTED_CONFIGURATIONS TARGET Qt5::Core PROPERTY IMPORTED_CONFIGURATIONS )
	if ( "DEBUG" IN_LIST QT_IMPORTED_CONFIGURATIONS )
		set_target_properties( Qt${QT_VERSION_MAJOR}::Core PROPERTIES MAP_IMPORTED_CONFIG_COVERAGE "DEBUG" )
		set_target_properties( Qt${QT_VERSION_MAJOR}::Network PROPERTIES MAP_IMPORTED_CONFIG_COVERAGE "DEBUG" )
	endif()
	MARK_AS_ADVANCED(
		CMAKE_CXX_FLAGS_COVERAGE
		CMAKE_C_FLAGS_COVERAGE
		CMAKE_EXE_LINKER_FLAGS_COVERAGE
		CMAKE_SHARED_LINKER_FLAGS_COVERAGE
		CMAKE_STATIC_LINKER_FLAGS_COVERAGE
	)

	# sanitize=alignment is disabled because hippomocks contains a violation and GCC isn't able to exclude it
	set( CMAKE_CXX_FLAGS_SANITIZE "${CMAKE_CXX_FLAGS_DEBUG} -O0 -fsanitize=address,undefined,leak -fno-omit-frame-pointer -fno-sanitize-recover -fno-sanitize=alignment" )
	set( CMAKE_C_FLAGS_SANITIZE "${CMAKE_C_FLAGS_DEBUG} -O0 -fsanitize=address,undefined,leak -fno-omit-frame-pointer -fno-sanitize-recover -fno-sanitize=alignment" )
	set( CMAKE_EXE_LINKER_FLAGS_SANITIZE "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -fsanitize=address,undefined,leak" )
	set( CMAKE_SHARED_LINKER_FLAGS_SANITIZE "${CMAKE_SHARED_LINKER_FLAGS_DEBUG} -fsanitize=address,undefined,leak" )
	set( CMAKE_STATIC_LINKER_FLAGS_SANITIZE "${CMAKE_STATIC_LINKER_FLAGS_DEBUG}" )
	get_property( QT_IMPORTED_CONFIGURATIONS TARGET Qt${QT_VERSION_MAJOR}::Core PROPERTY IMPORTED_CONFIGURATIONS )
	if ( "DEBUG" IN_LIST QT_IMPORTED_CONFIGURATIONS )
		set_target_properties( Qt${QT_VERSION_MAJOR}::Core PROPERTIES MAP_IMPORTED_CONFIG_SANITIZE "DEBUG" )
		set_target_properties( Qt${QT_VERSION_MAJOR}::Network PROPERTIES MAP_IMPORTED_CONFIG_SANITIZE "DEBUG" )
	endif()
	MARK_AS_ADVANCED(
		CMAKE_CXX_FLAGS_SANITIZE
		CMAKE_C_FLAGS_SANITIZE
		CMAKE_EXE_LINKER_FLAGS_SANITIZE
		CMAKE_SHARED_LINKER_FLAGS_SANITIZE
		CMAKE_STATIC_LINKER_FLAGS_SANITIZE
	)

elseif( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC" )

	add_compile_options( "/W3" )
	set_property( DIRECTORY "." APPEND PROPERTY COMPILE_DEFINITIONS "_UNICODE" "UNICODE" )

endif()

set( SONARCLOUD_TOKEN "$ENV{SONARCLOUD_TOKEN}" )
set( CI_COMMIT_REF_NAME "$ENV{CI_COMMIT_REF_NAME}" )
set( CI_PROJECT_ID "$ENV{CI_PROJECT_ID}" )
set( CI_COMMIT_SHA "$ENV{CI_COMMIT_SHA}" )
set( DASHED_PROJECT_VERSION_PRERELEASE "" )
if( NOT "${PROJECT_VERSION_PRERELEASE}" STREQUAL "" )
	set( DASHED_PROJECT_VERSION_PRERELEASE "-${PROJECT_VERSION_PRERELEASE}" )
endif()
configure_file( "${CMAKE_SOURCE_DIR}/sonar-project.properties.in"
	"${CMAKE_SOURCE_DIR}/sonar-project.properties"
	NEWLINE_STYLE UNIX
)

add_definitions( -DQT_DEPRECATED_WARNINGS )

add_subdirectory( doc )
enable_testing()
add_subdirectory( tests )


set( MOCKNETWORKACCESSMANAGER_LIBRARY_SOURCE_FILE "${CMAKE_SOURCE_DIR}/DummyMain.cpp" )
file( GENERATE OUTPUT "${MOCKNETWORKACCESSMANAGER_LIBRARY_SOURCE_FILE}" CONTENT "#include \"MockNetworkAccessManager.hpp\"\n" )
set_property( SOURCE "${MOCKNETWORKACCESSMANAGER_LIBRARY_SOURCE_FILE}" PROPERTY GENERATED TRUE )
add_library( MockNetworkAccessManager STATIC EXCLUDE_FROM_ALL "MockNetworkAccessManager.hpp" "${MOCKNETWORKACCESSMANAGER_LIBRARY_SOURCE_FILE}" )
target_link_libraries( MockNetworkAccessManager Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Network )

