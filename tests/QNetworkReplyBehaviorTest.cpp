/*! \file
 * \author Jochen Ulrich <jochenulrich@t-online.de>
 */

#include "QSignalInspector.hpp"

#include <QtDebug>
#include <QCoreApplication>
#include <QFile>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSharedPointer>
#include <QTextStream>
#include <QTimer>
#include <QUrl>

#include <cstdio>
#include <iostream>

QString escapeChar(QChar character)
{
	switch (character.unicode())
	{
	case '\0': return "\\0";
	case '\'': return "\\'";
	case '\"': return "\\\"";
	case '\\': return "\\\\";
	case '\a': return "\\a";
	case '\b': return "\\b";
	case '\f': return "\\f";
	case '\n': return "\\n";
	case '\r': return "\\r";
	case '\t': return "\\t";
	case '\v': return "\\v";
	default: break;
	}

	if (!character.isPrint())
	{
		return "\\x" + QString::number(character.unicode(), 16);
	}
	else
		return character;
}

QString escapeString(const QString& string)
{
	QString result;

	for (int i=0; i < string.size(); ++i)
	{
		QChar c = string[i];

		if (c.isHighSurrogate() || c.isLowSurrogate())
		{
			if (i < (string.size() - 1))
			{
				QString surrogatePair = QString(string[i]) + string[i+1];
				result += "\\u" + QString::number(surrogatePair.toUcs4().first(), 16).rightJustified(8, '0');
				i += 1;
			}
		}
		else
			result += escapeChar(c);
	}

	return result;
}

QSharedPointer<QNetworkReply> triggerRequest(QNetworkAccessManager::Operation op,
                                             const QNetworkRequest& req,
                                             QNetworkAccessManager& qNam,
                                             const QByteArray& data = QByteArray())
{
	QSharedPointer<QNetworkReply> reply;

	switch (op)
	{
	case QNetworkAccessManager::GetOperation: reply.reset(qNam.get(req)); break;
	case QNetworkAccessManager::HeadOperation: reply.reset(qNam.head(req)); break;
	case QNetworkAccessManager::PostOperation: reply.reset(qNam.post(req, data)); break;
	case QNetworkAccessManager::PutOperation: reply.reset(qNam.put(req, data)); break;
	case QNetworkAccessManager::DeleteOperation: reply.reset(qNam.deleteResource(req)); break;
	}

	return reply;
}

void logHeaders( QTextStream& out, const QSharedPointer<QNetworkReply>& reply )
{
	out << "Headers:" << endl;
	for( auto&& header : reply->rawHeaderList() )
	{
		out << "\t" << header << ": " << reply->rawHeader( header ) << "\n";
	}
}

void logAttributes( QTextStream& out, const QSharedPointer<QNetworkReply>& reply )
{
	out << "Attributes:" << endl;
	for( QNetworkRequest::Attribute attribute = QNetworkRequest::RedirectionTargetAttribute; attribute <= QNetworkRequest::Http2DirectAttribute; )
	{
		if( !reply->attribute( attribute).isNull() )
		{
			out << "\t" << attribute << ": " << reply->attribute( attribute ).toString() << "\n";
		}

		attribute = static_cast<QNetworkRequest::Attribute>( static_cast<int>(attribute) + 1 );
	}
}

void logReplyInfos( QTextStream& out, const QSharedPointer<QNetworkReply>& reply )
{
	out << "Reply from: " << reply->url().toString() << endl;
	out << reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toString() << " " << reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toString() << endl;
	out << "Finished: " << ( reply->isFinished() ? "true" : "false" ) << endl;
	if( !reply->header( QNetworkRequest::ContentLengthHeader ).isNull() )
		out << "Content-Length: " << reply->header( QNetworkRequest::ContentLengthHeader ).toInt() << endl;
	if (reply->error() != QNetworkReply::NoError)
		out << "Error: " << reply->error() << " " << reply->errorString() << endl;
	logHeaders( out, reply );
	logAttributes( out, reply );
	out << "Body:" << endl << reply->readAll() << endl;
}

QSharedPointer<QSignalInspector> executeRequest(QTextStream& out,
                                                QNetworkAccessManager::Operation op,
                                                const QNetworkRequest& req,
                                                QNetworkAccessManager& qNam,
                                                const QByteArray& data = QByteArray())
{
	auto reply = triggerRequest(op, req, qNam, data);
	auto inspector = QSharedPointer<QSignalInspector>::create(reply.data());

	if ( reply->isRunning() )
	{
		QEventLoop eventLoop;
		QObject::connect( reply.data(), &QNetworkReply::finished, &eventLoop, &QEventLoop::quit );
		QTimer timer;
		timer.setSingleShot( true );
		timer.callOnTimeout( [ &out, reply ]() {
			logReplyInfos( out, reply );
		} );
		timer.start( 1000 );
		QObject::connect( reply.data(), &QNetworkReply::finished, &timer, &QTimer::stop );
		eventLoop.exec();
	}

	logReplyInfos( out, reply );
	return inspector;
}

QSharedPointer<QSignalInspector> executeAndAbortRequest(QTextStream& out,
                                                        QNetworkAccessManager::Operation op,
                                                        const QNetworkRequest& req,
                                                        QNetworkAccessManager& qNam,
                                                        const QByteArray& data = QByteArray())
{
	auto reply = triggerRequest(op, req, qNam, data);
	auto inspector = QSharedPointer<QSignalInspector>::create(reply.data());

	if ( reply->isRunning() )
	{
		QEventLoop eventLoop;
		eventLoop.processEvents( QEventLoop::AllEvents, 100 );
		reply->abort();
		eventLoop.processEvents( QEventLoop::AllEvents, 1000 );
	}

	logReplyInfos( out, reply );
	return inspector;
}

void printSignals(QTextStream& out, QSharedPointer<QSignalInspector> inspector)
{
	for (auto signalEmission : *inspector)
	{
		QStringList parameterValues;
		for (const auto& parameter : signalEmission.parameters)
		{
			QString valueStr;
			if (parameter.canConvert<QString>())
			{
				valueStr = parameter.toString();
				QMetaType::Type type = static_cast<QMetaType::Type>(parameter.type());
				if (   type == QMetaType::QString
				    || type == QMetaType::QByteArray)
				{
					valueStr = QStringLiteral("\"%1\"").arg(escapeString(valueStr));
				}
				else if (   type == QMetaType::QChar
				         || type == QMetaType::Char)
				{
					valueStr = QStringLiteral("'%1'").arg(escapeChar(valueStr.at(0)));
				}
			}
			else
				valueStr = QString::fromUtf8(parameter.typeName());
			parameterValues << valueStr;
		}
		out << "[" << signalEmission.timestamp.toString("yyyy-MM-ddTHH:mm:ss.zzz") << "] " << signalEmission.signal.name() << "(" << parameterValues.join(", ") << ")" << endl;
	}
}

QNetworkRequest prepareRequest(QNetworkAccessManager::Operation op,
                               const QString& url,
                               QTextStream& out,
                               const QByteArray& contentType = QByteArray())
{
	QString verb;
	switch (op)
	{
	case QNetworkAccessManager::GetOperation: verb = "GET"; break;
	case QNetworkAccessManager::HeadOperation: verb = "HEAD"; break;
	case QNetworkAccessManager::PostOperation: verb = "POST"; break;
	case QNetworkAccessManager::PutOperation: verb = "PUT"; break;
	case QNetworkAccessManager::DeleteOperation: verb = "DELETE"; break;
	}

	out << verb << " " << url << endl;
	out << QString{verb.size() + 1 + url.size(), '-'} << endl;

	QNetworkRequest req{QUrl{url}};
#if QT_VERSION >= QT_VERSION_CHECK( 5,6,0 )
	req.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
	req.setMaximumRedirectsAllowed(4);
#endif // Qt >= 5.6.0
	if (!contentType.isEmpty())
		req.setHeader(QNetworkRequest::ContentTypeHeader, contentType);

	return req;
}

void executeAndPrint(QNetworkAccessManager::Operation op,
                     const QString& url,
                     QTextStream& out,
                     QNetworkAccessManager& qNam,
                     const QByteArray& body = QByteArray(),
                     const QByteArray& contentType = QByteArray())
{
	auto request = prepareRequest(op, url, out, contentType);
	printSignals(out, executeRequest(out, op, request, qNam, body));
	out << endl;
}

void executeAbortAndPrint(QNetworkAccessManager::Operation op,
                           const QString& url,
                           QTextStream& out,
                           QNetworkAccessManager& qNam,
                           const QByteArray& body = QByteArray(),
                           const QByteArray& contentType = QByteArray())
{
	auto request = prepareRequest(op, url, out, contentType);
	printSignals(out, executeAndAbortRequest(out, op, request, qNam, body));
	out << endl;
}


int main(int argc, char **argv)
{
	QCoreApplication app{argc, argv};
	QTextStream out{stdout};
	QNetworkAccessManager qNam;

	QByteArray jsonData = "{ \"random\": \"gLINDuERfaRBQesd6FuF2DJ0vMaVvHDlCPm9ib2DnIIrgSwVSYQdlb5RvUiK89EZIJMeCQOk1iRRDYSiCN8sdpvehD9JP1eKWxfTzZtLLUlAEVVKBqmz4oheyJYcLpWxNnxPlAUHmJ3I09ZYli99sF9wM1Amhjnj4vKwWuWSJRtsQZrJdMDdWteUjnxQQy4SyTzVoXlFxljLu96WAc7kOPREd9brDzyfVGxOJ79cnwO8M3wBLExAwVX4fdLf9qbPz0UsErefH9t1Sy2hVeGFKzeewRLN97djADR\" }";

	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/status/200", out, qNam);
	/*
	executeAndPrint(QNetworkAccessManager::GetOperation, "file:///C:/Temp/read-only.txt", out, qNam);
	executeAndPrint(QNetworkAccessManager::HeadOperation, "file:///C:/Temp/read-only.txt", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "file:///C:/Temp/empty.txt", out, qNam);
	executeAndPrint(QNetworkAccessManager::HeadOperation, "file:///C:/Temp/empty.txt", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "file:///C:/Temp/missing.txt", out, qNam);
	executeAndPrint(QNetworkAccessManager::HeadOperation, "file:///C:/Temp/missing.txt", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "qrc:/missing.txt", out, qNam);
	executeAndPrint(QNetworkAccessManager::PostOperation, "file:///C:/Temp/empty.txt", out, qNam, "foo bar", "text/plain");
	executeAndPrint(QNetworkAccessManager::PutOperation, "file:///C:/Temp/empty.txt", out, qNam, "foo bar", "text/plain");
	executeAndPrint(QNetworkAccessManager::PutOperation, "file:///C:/Temp/empty.txt", out, qNam, QByteArray(), "text/plain");
	executeAndPrint(QNetworkAccessManager::PutOperation, "file:///C:/Temp/created.txt", out, qNam, "foo bar", "text/plain");
	executeAndPrint(QNetworkAccessManager::PutOperation, "file:///C:/Temp/read-only.txt", out, qNam, "foo bar", "text/plain");
	executeAndPrint(QNetworkAccessManager::DeleteOperation, "file:///C:/Temp/empty.txt", out, qNam);
	executeAndPrint(QNetworkAccessManager::DeleteOperation, "qrc:/test.txt", out, qNam);
	executeAndPrint(QNetworkAccessManager::PutOperation, "qrc:/test.txt", out, qNam, "foo bar", "text/plain" );
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/status/400", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/status/500", out, qNam);
	executeAndPrint(QNetworkAccessManager::PostOperation, "http://eu.httpbin.org/status/500", out, qNam, "foo bar", "text/plain");
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/get", out, qNam);
	executeAndPrint(QNetworkAccessManager::PostOperation, "http://eu.httpbin.org/post", out, qNam, jsonData, "application/json");
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/redirect/3", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/redirect-to?url=http%3A%2F%2Fhttpbin.org%2Fstatus%2F404", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/redirect-to?url=foo%3A%2F%2Fbar", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/redirect-to?url=http%3A%2F%2F%24", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/redirect-to?url=%2Fget", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/redirect-to?url=%2F%2Fhttpbin.org%2Fget", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/delay/3", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/drip?duration=5&code=500&numbytes=5", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/stream/20", out, qNam);
	executeAndPrint(QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/redirect/5", out, qNam);

	executeAbortAndPrint( QNetworkAccessManager::GetOperation, "http://eu.httpbin.org/status/200", out, qNam );
	*/

	return 0;
}
