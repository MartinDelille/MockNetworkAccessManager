import qbs
import qbs.Environment

Project {

	qbsSearchPaths: [
		"qbs"
	]

	references: [
		"tests.qbs"
	]

	AutotestRunner {
		Depends { name: "Qt.core" }
		environment: qbs.targetOS.contains( "windows" ) ? [ "PATH=" + Qt.core.binPath + qbs. pathListSeparator + Environment.getEnv( "PATH" ) ] : base
	}

}