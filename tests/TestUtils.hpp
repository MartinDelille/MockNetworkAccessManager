#ifndef MOCKNETWORKACCESSMANAGER_TESTS_TESTUTILS_HPP
#define MOCKNETWORKACCESSMANAGER_TESTS_TESTUTILS_HPP

#include "QSignalInspector.hpp"

#include <QPair>
#include <QVariant>
#include <QMetaMethod>
#include <QList>
#include <QRegularExpression>
#include <QTest>

namespace Tests {

typedef QPair<QMetaMethod, QList<QVariant> > SignalEmission;
typedef QList<SignalEmission> SignalEmissionList;

QMetaMethod getSignal( const QMetaObject& metaObj, const char* signal )
{
	const int signalIndex = metaObj.indexOfSignal( QMetaObject::normalizedSignature( signal ).data() );
	return metaObj.method( signalIndex );
}

SignalEmissionList getSignalEmissionListFromInspector(const QSignalInspector& inspector)
{
	const QStringList signalBlacklist = (QStringList() <<
	                                     "startHttpRequest");
	SignalEmissionList emissions;
	QSignalInspector::ConstIterator iter;
	for (iter = inspector.cbegin(); iter != inspector.cend(); ++iter)
	{
		if (!signalBlacklist.contains(iter->signal.name()))
			emissions << qMakePair(iter->signal, iter->parameters);
	}
	return emissions;
}

void ignoreMessage( QtMsgType type, const QString& message )
{
	#if QT_VERSION < QT_VERSION_CHECK( 5,3,0 )
		// Workaround for https://bugreports.qt.io/browse/QTBUG-15256
		QTest::ignoreMessage( type, ( message + " " ).toUtf8().constData() );
	#else
		QTest::ignoreMessage( type, message.toUtf8().constData() );
	#endif
}

void ignoreMessage( QtMsgType type, const QRegularExpression& regex )
{
	#if QT_VERSION >= QT_VERSION_CHECK( 5,3,0 )
		QTest::ignoreMessage( type, regex );
	#else
		Q_UNUSED( type )
		Q_UNUSED( regex )
	#endif
}

} // namespace Tests

Q_DECLARE_METATYPE(Tests::SignalEmissionList)


#endif // MOCKNETWORKACCESSMANAGER_TESTS_TESTUTILS_HPP
