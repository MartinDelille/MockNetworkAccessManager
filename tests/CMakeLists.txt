
find_package( Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core Network Test )
get_property( QT_IMPORTED_CONFIGURATIONS TARGET Qt${QT_VERSION_MAJOR}::Core PROPERTY IMPORTED_CONFIGURATIONS )
if ( "DEBUG" IN_LIST QT_IMPORTED_CONFIGURATIONS )
	set_target_properties( Qt${QT_VERSION_MAJOR}::Test PROPERTIES MAP_IMPORTED_CONFIG_COVERAGE "DEBUG" )
endif()

include_directories( ${PROJECT_SOURCE_DIR} )

# Disable inlining because it breaks HippoMocks
if( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC" )
	add_compile_options( /Ob0 )
else()
	add_compile_options( -fno-inline )
endif()

function( setupStandardTest TESTNAME )
	add_executable( ${TESTNAME} ${TESTNAME}.cpp "${PROJECT_SOURCE_DIR}/MockNetworkAccessManager.hpp" "QSignalInspector.hpp" )
	target_link_libraries( ${TESTNAME} Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Network Qt${QT_VERSION_MAJOR}::Test )
	add_test( NAME ${TESTNAME} COMMAND ${TESTNAME} )
endfunction()

setupStandardTest( RuleTest )
setupStandardTest( PredicateTest )
setupStandardTest( HttpUtilsTest )
setupStandardTest( HttpAuthenticationTest )
setupStandardTest( MockReplyTest )
setupStandardTest( ManagerTest )
setupStandardTest( VersionNumberTest )

add_executable( MyNetworkClientTest MyNetworkClientTest.cpp "${PROJECT_SOURCE_DIR}/MockNetworkAccessManager.hpp" "MyNetworkClient.hpp" )
target_link_libraries( MyNetworkClientTest Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Network Qt${QT_VERSION_MAJOR}::Test )
add_test( NAME MyNetworkClientTest COMMAND MyNetworkClientTest )

add_executable( CompatibilityTest CompatibilityTest.cpp "${PROJECT_SOURCE_DIR}/MockNetworkAccessManager.hpp" )
target_compile_definitions( CompatibilityTest PRIVATE
	QT_NO_CAST_TO_ASCII
	QT_NO_CAST_FROM_ASCII
	QT_NO_CAST_FROM_BYTEARRAY
	QT_NO_KEYWORDS
)
if( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" )
	target_compile_options( CompatibilityTest PRIVATE "-Wconversion" )
endif()
target_link_libraries( CompatibilityTest Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Network Qt${QT_VERSION_MAJOR}::Test )
add_test( NAME CompatibilityTest COMMAND CompatibilityTest )
include( CheckCXXCompilerFlag )
CHECK_CXX_COMPILER_FLAG( "-std=c++11" COMPILER_SUPPORTS_CXX11 )
if( COMPILER_SUPPORTS_CXX11 AND ( Qt${QT_VERSION_MAJOR}Core_VERSION VERSION_LESS 6.0.0 ) )
	target_compile_options( CompatibilityTest PRIVATE -std=c++11 )
endif()

