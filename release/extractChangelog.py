import re
import argparse
from pathlib import Path

def main():
	args = parseCommandLine()
	changelog = args.file.read_text()
	section = extractVersionSection( changelog, args )
	if section is not None:
		if args.omit_heading:
			section = section.split( "\n", 1 )[ 1 ]
		print( section.strip() )


def parseCommandLine():
	argParser = argparse.ArgumentParser( description='''
		Extracts a version section from a changelog file in Keep-a-Changelog format.''' )

	argParser.add_argument( 'file', type=Path, help='Path to the changelog file.' )
	argParser.add_argument( 'version', help='The version number whose section is extracted. ' +
	                        'Must match exactly.' )
	argParser.add_argument( '--omit-heading', action='store_true',
	                        help='Removes the version heading from the output.' )

	return argParser.parse_args()

def extractVersionSection( changelog, args ):
	changelogSections = re.split( r"\s*---\s*", changelog )
	for changelogSection in changelogSections:
		if re.search( r"## [?{args.version}]?", changelogSection ):
			return changelogSection
	return None
	


if __name__ == '__main__':
	main()

